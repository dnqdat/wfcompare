$(document).ready(function() {
	WFCompare.run();
});


var WFCompare = {
	config: {
		pc: {
			keys: 'CTR + ?',
			icon: 'fa fa-desktop',
			container: '.container',
		},
		sp: {
			keys: 'CTR + ?', 
			icon: 'fa fa-mobile',
			container: '.container',
		},
		pc_menu: {
			keys: 'CTR + ?', 
			icon: 'fa fa-bars',
			text: 'P',
			container: '.menu-container'
		},
		sp_menu: {
			keys: 'CTR + ?', 
			icon: 'fa fa-bars',
			text: 'S',
			container: '.menu-container'
		}
	},

	renderDevTools: function() {
		var that = this;
		var htmlBtns = Object.keys(this.config).reduce(function(htmlBtns, key) {
			var btnConfig = that.config[key];
			htmlBtns += `
			 <button data-target="${key}" class="btn btn-danger">
			  	<i class="${that.config[key].icon}" aria-hidden="true"></i>
			  	${btnConfig.text ? btnConfig.text : ''}
			  </button>`
		  return htmlBtns;
		}, '')
		var html = `
			<div id="wfCompareDevTools" class="btn-group-vertical btn-group-xs" role="group">
				${htmlBtns}
			</div>
		`;

		$('body').append(html);

		var $buttons = $("#wfCompareDevTools button");

		$buttons.click(function() {
			var target = $(this).attr('data-target');
			that.toggleWF(target);
			$buttons.not(this).removeClass('.active');
			$(this).toggleClass('active');
		});
	},

	getWFImage: function(target = 'pc') {
		if(target.indexOf('menu') !== -1) {
			var img = `${target}.jpg`;
		} else {
			var baseImg = $("head title").text().toLowerCase();
			baseImg = baseImg.replace(/\/| |\?/g, "_");
			var img = `${target}_${baseImg}.jpg`;
		}
		
		img  = '../assets/images/wf/' + img;
		return img;
	},

	toggleWF: function(target = 'pc') {
		var $wfCompare = $("#wfCompare");
		var curTarget = $wfCompare.attr('data-target');
		if($wfCompare.length > 0) {
				$('.wfcompare').remove();

			if(curTarget === target) return true;
		}
		var config = this.config[target];
		var imgSrc = this.getWFImage(target);
		var html = `
			<div id="wfCompare" class="wfcompare" data-target="${target}">
	      <img src="${imgSrc}" alt="${target}">
	    </div>
		`;
		$(`${config.container}`).append(html);
	},

	setupKeys: function() {
		var iShortCutControlKey = 17; 
		var isSecondaryKey=191;
		var bIsControlKeyActived = false;

		$(document).keyup(function(e) {
		    if (e.which == iShortCutControlKey) {
		    	bIsControlKeyActived = false;
		    	$("#wfCompare").hide();
		    }

		     if (e.which == isSecondaryKey) {
		     	$("#wfCompare").hide();
		     }
		}).keydown(function(e) {
		    if (e.which == iShortCutControlKey) bIsControlKeyActived = true;
		    if (bIsControlKeyActived == true) {
		   		if (e.which == isSecondaryKey) {
		   			$("#wfCompare").show();
		   		}
		    }
		});
	},

	setup: function() {
		this.renderDevTools();
		this.setupKeys();
	},

	run: function(option) {
		this.setup();
	}
}